chrome.runtime.onInstalled.addListener(() => {
    chrome.storage.local.set({ opendyslexic: true});
    chrome.storage.local.set({ alignleft: true});
    chrome.storage.local.set({ enable: true});
    chrome.storage.local.set({ mask: false});
});