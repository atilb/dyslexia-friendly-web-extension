let isFormDisplayed = true;

window.addEventListener('DOMContentLoaded', () => {
    InitializePopupEvents();
    SetDyslexiaProperties();
    TranslatePopupStrings();
});

const SetDyslexiaProperties = () => {
    const RefreshPopupFormValue = (setting) => {
        if(setting.propName == "enable")
        {
            chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
                let url = new URL(tabs[0].url);
                chrome.storage.local.get(url.hostname, (it) => {
                    document.getElementById(setting.propHtmlId).checked = !it[url.hostname];
                    if(!document.getElementById(setting.propHtmlId).checked) {
                        document.getElementsByClassName("hideable")[0].classList.add("hard-darkener");
                    }
                });
            });
        }
        else 
        {
            chrome.storage.local.get(setting.propName, (items) => {
                document.getElementById(setting.propHtmlId).checked = items[setting.propName]; 
            });
        }
    }
    
    const SettingFormChangeEvent = (setting) =>  {
        document.getElementById(setting.propHtmlId).addEventListener('change', () => {
            if(setting.propName == "enable")
            {
                chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
                    let url = new URL(tabs[0].url);
                    if(document.getElementById(setting.propHtmlId).checked) {
                        chrome.storage.local.set({[url.hostname]: false});
                        document.getElementsByClassName("hideable")[0].classList.remove("hard-darkener");
                    }
                    else {
                        chrome.storage.local.set({[url.hostname]: true});
                        document.getElementsByClassName("hideable")[0].classList.add("hard-darkener");
                    }
                });    
            }
            else 
            {
                chrome.storage.local.set({[setting.propName]: document.getElementById(setting.propHtmlId).checked}, () => { });
            }
            // refresh content script
            chrome.tabs.query({currentWindow: true}, function(tabs) {
                tabs.forEach(function(tab) {
                    chrome.tabs.executeScript(tab.id, { file: 'js/content.js'}, _=> chrome.runtime.lastError);
                });
            });
        });
    }

    const DyslexiaProperties = [
        { propName:'opendyslexic', propHtmlId:'check-replace-font'}, 
        { propName:'alignleft', propHtmlId:'check-text-alignLeft'},
        { propName:'enable', propHtmlId:'check-enable'},
        { propName:'mask', propHtmlId:'check-text-mask'},
        // { propName:'letterspace', propHtmlId:'slider-letter-space' }
    ];

    DyslexiaProperties.forEach((item) => {
        RefreshPopupFormValue(item);
        SettingFormChangeEvent(item);
    });
}

//https://stackoverflow.com/questions/57566463/clarification-on-i18n-implementation-for-chromium-extensions
const TranslatePopupStrings = () => {
    let els = document.querySelectorAll('[data-locale]');
    els.forEach(el => el.innerText = chrome.i18n.getMessage(el.dataset.locale));
}

const InitializePopupEvents = () => {
    document.getElementById("form-button").addEventListener("click", () => {
        document.getElementById("form-container").style.display = "block";
        document.getElementById("details-container").style.display = "none";
    });

    document.getElementById("details-button").addEventListener("click", () => {
        document.getElementById("form-container").style.display = "none";
        document.getElementById("details-container").style.display = "block";
    });

    document.getElementById("mail-row").addEventListener("click", () => {
        window.open('mailto:dyslexiatoolbox@protonmail.com','_blank');
    });

    document.getElementById("website-row").addEventListener("click", () => {
        window.open('https://www.arnaudtilbian.com','_blank');
    });

    document.getElementById("rate-section").addEventListener("click", () => {
        window.open('https://chrome.google.com/webstore/detail/dylsexia-toolbox/gpjbfckicpmdjmhgcjoecomooejngkam?utm_source=chrome-ntp-icon','_blank');
    });
}