AddProperty = (propertyName) => {
    let t = document.createElement('link');
    t.rel = 'stylesheet';
    t.type = 'text/css';
    t.setAttribute('class', propertyName);
    t.href = chrome.runtime.getURL('css/client_css_style/'+propertyName+'.css');
    document.head.appendChild(t);

    if(propertyName == "mask")
    {
        AddMask();
    }
}

RemoveProperty = (propertyName) => {
    let links = document.getElementsByClassName(propertyName);
    Array.prototype.forEach.call(links, element => element.remove());

    if(propertyName == "mask")
    {
        RemoveMask();
    }
}

UpdateProperty = (propertyName) => {
    chrome.storage.local.get(window.location.hostname, (hosts) => {
        chrome.storage.local.get(propertyName, (items) => {
            if(items[propertyName])
            {
                if(hosts[window.location.hostname])
                {
                    RemoveProperty(propertyName);
                }
                else {
                    if(document.getElementsByClassName(propertyName).length == 0) {
                        AddProperty(propertyName);
                    }
                }
            }
            else
            {
                RemoveProperty(propertyName);
            }
        });
    });
}

AddMask = () => {
    let top = document.createElement("div");
    top.id = "mask-top";
    document.body.appendChild(top);
    
    let bottom = document.createElement("div");
    bottom.id = "mask-bottom";
    document.body.appendChild(bottom);

    document.addEventListener("mousemove", MouseMoveEvent);
    DrawMask(); // first draw is manual
}

DrawMask = (mouseYPos) => {
    let areaHeight = 150;
    let top = document.getElementById("mask-top");
    let bottom = document.getElementById("mask-bottom");
    
    if(top == null || bottom == null)
        return;

    if(mouseYPos + areaHeight/2 > window.innerHeight)
        bottom.style.height = "0px";
    else if(mouseYPos - areaHeight/2 < 0)
        top.style.height = "0px";

    top.style.height = mouseYPos - areaHeight/2 + "px";
    bottom.style.height = window.innerHeight - mouseYPos - areaHeight/2 + "px";
}

MouseMoveEvent = (event) => {
    DrawMask(event.clientY); 
}

RemoveMask = () => {
    document.removeEventListener("mousemove", MouseMoveEvent);
    let top = document.getElementById("mask-top");
    let bottom = document.getElementById("mask-bottom");
    if(top != null)
        document.body.removeChild(top);
    if(bottom != null)
        document.body.removeChild(bottom);
}

UpdateProperty('alignleft');
UpdateProperty('opendyslexic');
UpdateProperty("mask");